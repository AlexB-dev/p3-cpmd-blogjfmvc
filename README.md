OpenClassRooms Parcours Chef de Projet Multimedia - DÉVELOPPEMENT - Alexandre BLONDEL
Projet N°3 - Blog d'un ecrivain
Installation

Importer la base de donnée jointe depuis phpmyadmin (par exemple) sur votre serveur web
Configurer le fichier Model.php dans le repertoire suivant --> /models/Model.php en fonction de vos id pour la bdd
Copier les Sources dans le répertoire de votre serveur web 


FRONT:

Visualisation des Billets (Episodes du livre)
Ajout d'un commentaire ou d'une réponse à un commentaire
Signaler un commentaire

BACKOFFICE:

Mise à jour de la page du billet (TinyMce) et input pour le titre (pour l'id il est en hidden auto increment)
Ajout d'un nouveau Post depuis le bouton rond avec un plus sur la mage d'admin(TinyMce)

Publication immédiate
Suppression d'un Billet
Gestion des signalements avec une validation depuis le bouton "Moderation commentaire"
Déconnexion de l'utilisateur depuis son bouton

ps : pour testé la page d'admin voici les log de l'utilisateur créé dans la bdd que je fourni

login : jeanf
mdp : adminpass


BLONDEL Alexandre






