<?php
require_once 'models/Comment.php';
require_once 'models/Post.php';

class ControllerModerationPost {
	private $commentaires;

	function __construct(){
		$this->commentaires = new Commentaire;
	}
	//récuêre les commentaires a validé
	function getModerateCommentaires(){
		$commentaires = $this->commentaires->getModerateCommentaires();
		$vue = new View("ModerationPost");
		$vue->generer(array('commentaires' => $commentaires));
	}	
	//sauvegarde du statut de commentaire
	function upStatCommentaire($idCommentaire){
		$this->commentaires->upStatCommentaire($idCommentaire);
	//refresh de l'affichage du Post
    $this->getModerateCommentaires();
	}
}