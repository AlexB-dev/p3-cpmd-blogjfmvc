<?php
require_once 'controllers/ControllerFront.php';
require_once 'controllers/ControllerBack.php';
require_once 'controllers/ControllerAccueil.php';
require_once 'views/view.php';

class Router {

  private $ctrlAccueil;
  private $ctrlBack;
  private $ctrlFront;

  // Traite une requête entrante
  public function routerRequete($args) {
  if (!isset($args["seg1"]))$args["seg1"] ="";
    switch ($args["seg1"]){
      case "article" :
      $this->ctrlFront = new ControllerFront($args);
      $this->ctrlFront->do();
      break;
      case "admin" : 
      $this->ctrlBack = new ControllerBack($args);
      $this->ctrlBack->do();
      break;
      default : 
      $this->ctrlAccueil = new ControllerAccueil();
      $this->ctrlAccueil->accueil();
      break; 
      }
 }
  // Affiche une erreur
  private function erreur($msgErreur){
    $vue = new View("Erreur");
    $vue->generer(array('msgErreur' => $msgErreur));
  }

  // Recherche un paramètre dans un tableau
  private function getParametre($tableau, $nom) {
    if (isset($tableau[$nom])) {
      return $tableau[$nom];
    }
    else
      throw new Exception("Paramètre '$nom' absent");
  }
}