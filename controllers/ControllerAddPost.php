<?php

	require_once 'models/Post.php';
	require_once 'views/view.php';


	class ControllerAddPost {

			 private $post;

			 public function __construct(){
			 	$this->post = new Post();
			 }

		  // Affiche la liste de tous les Posts du blog
			  public function Post() {
			    $posts = $this->post->getPosts();
			    $vue = new View("AdminPost");
			    $vue->generer(array('posts' => $posts));
			  }
			//Ajout d'un Post
			  public function addPost($titre, $contenu){
			  	// sauvegarde du Post
			  	$this->post->addPost($titre, $contenu);
			  	//Refresh affichage du bilet
			  	$this->Post();
			  }
			//Affichage de l'editeur
				public function editeurPost() {
					$vue= new View("AddPost");
					$vue->generer(array('editeur'));

				} 		
			}

	