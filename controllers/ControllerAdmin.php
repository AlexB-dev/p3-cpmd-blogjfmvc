<?php

require_once 'models/Post.php';
require_once 'models/Comment.php';
require_once 'views/view.php';

class ControllerAdmin {

  private $Post;
  private $commentaire;

   // Affiche la liste de tous les Posts du blog
  public function adminPost() {
    $Post = new Post();
    $Posts = $Post->getPosts();
    $vue = new View("AdminPost");
    $vue->generer(array('Posts' => $Posts));
  }

  // Affiche les détails sur un Post
  public function Post($idPost) {
    $Post = new Post();
    $commentaire = new Commentaire();
    $Post = $Post->getPost($idPost);
    $commentaires = $commentaire->getCommentaires($idPost);
    $vue = new View("AdminPost");
    $vue->generer(array('Post' => $Post, 'commentaires' => $commentaires));
  }
}