<?php
require_once 'views/view.php';
require_once 'controllers/ControllerUsers.php';
require_once 'controllers/ControllerUsers.php';
require_once 'controllers/ControllerAddPost.php';
require_once 'controllers/ControllerUpPost.php';
require_once 'controllers/ControllerDelPost.php';
require_once 'controllers/ControllerAddPost.php';
require_once 'controllers/ControllerModerationPost.php';

class ControllerBack{

  private $data;
  private $ctrlAccueil;
  private $ctrlUsers;
  private $ctrlUpPost;
  private $ctrlAddPost;
  private $ctrlDelPost;
  private $ctrlModerationPost;



  function __construct($data){
  $this->data               = $data;
  $this->ctrlAccueil        = new ControllerAccueil;
  $this->ctrlUsers          = new ControllerUsers;
  $this->ctrlUpPost         = new ControllerUpPost;
  $this->ctrlAddPost        = new ControllerAddPost;
  $this->ctrlDelPost        = new ControllerDelPost;
  $this->ctrlModerationPost = new ControllerModerationPost;
 }

  function do(){
    switch ($this->data["seg2"]){
    case "connexion":
    if(!isset($this->data['login'])){
         $this->data['login'] = "";
         $this->data['pass']  = "";
         }
         $this->ctrlUsers->connecte($this->data['login'],$this->data['pass']);
         break;
   case "deconnexion":
        $this->ctrlUsers->deconnecte();
        break; 
   case "addpost":
        $titre = $this->data['titre'];
        $contenu = $this->data['contenu'];
        $this->ctrlAddPost->addPost($titre, $contenu);
        break;
   case "uppost":
        $idPost = $this->data['id'];
        $titre = $this->data['titre'];
        $contenu = $this->data['contenu'];
        $this->ctrlUpPost->upPost($idPost, $titre, $contenu);
        break;  
   case 'delpost':
        $idPost = $this->data['id'];
        $this->ctrlDelPost->delPost($idPost);
        break;
   case "editionuppost":
        $idPost = $this->data['id'];
        if($idPost !=0){
        $this->ctrlUpPost->editeurPost($idPost);
        }else{
        throw new Exception("Identifiant du Post non valide");
        }
        break; 
   case "editionaddpost":
        $this->ctrlAddPost->editeurPost();
        break;     
   case "moderationpost":
        $this->ctrlModerationPost->getModerateCommentaires(); 
        break; 
   case "upstatcomm":
        $idCommentaire = $this->getParametre($_POST, 'id');
        $this->ctrlModerationPost->upStatCommentaire($idCommentaire);
        break;
   default : 
        $this->ctrlAccueil->accueil();
        break; 
  }
 }
   // Recherche un paramètre dans un tableau
  private function getParametre($tableau, $nom) {
    if (isset($tableau[$nom])) {
      return $tableau[$nom];
    }
    else
      throw new Exception("Paramètre '$nom' absent");
  }
}
