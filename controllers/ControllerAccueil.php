<?php

require_once 'models/Post.php';
require_once 'views/view.php';

class ControllerAccueil {

  private $post;

  public function __construct() {
    $this->post = new Post();
  }

  // Affiche la liste de tous les Posts du blog
  public function accueil() {
    $posts = $this->post->getPosts();
    $vue = new View("Accueil");
    $vue->generer(array('posts' => $posts));
  }
}

