<?php
require_once 'models/Users.php';
require_once 'models/Post.php';
require_once 'views/view.php';

class ControllerUsers{
    
    public function __construct() {
    $this->post = new Post();
    $this->user = new Users();
  }
    
    public function connecte($login, $pass){
        if($this->user->verifUser($login, $pass) === true){
            $posts = $this->post->getPosts();
            $_SESSION['Auth'] = true;
            $vue = new View ("AdminPost");
            $vue->generer(array('posts' => $posts));
        }
        else {
            $vue = new View ("Log");
            $vue->generer(array());
        }
    }
    
    public function deconnecte(){
            $_SESSION['Auth'] = false;
            $vue = new View ("LogOut");
            $vue->generer(array());
    }
  }