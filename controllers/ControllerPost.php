<?php

require_once 'models/Post.php';
require_once 'views/view.php';
require_once 'models/Comment.php';

class ControllerPost {

  private $post;
  private $commentaire;

  public function __construct() {
    $this->post      = new Post();
    $this->commentaire = new Commentaire();
  }

  // Affiche les détails sur un Post
  public function post($idPost) {
    $commentaire = new Commentaire();
    $post = $this->post->getPost($idPost);
    $commentaires = $this->commentaire->getCommentaires($idPost);
    $vue = new View("Post");
    $vue->generer(array('post' => $post, 'commentaires' => $commentaires));
  }

  // Ajoute un commentaire à un Post
  public function commenter($auteur, $contenu, $idPost) {
    // Sauvegarde du commentaire
    $this->commentaire->ajouterCommentaire($auteur, $contenu, $idPost); 
    // Actualisation de l'affichage du Post
    $this->post($idPost);
  }
    //sauvegarde du statut de commentaire (signaler)
  function upStatCommSignal($idCommentaire, $idPost){
    $this->commentaire->upStatCommSignal($idCommentaire);
    $this->post($idPost);
  }
}