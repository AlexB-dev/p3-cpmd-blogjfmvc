<?php

	require_once 'models/Post.php';
	require_once 'views/view.php';


	class ControllerUppost{

			private $post;

			  public function __construct(){
			    $this->post = new Post();

			  }	
		  // Affiche la liste de tous les posts du blog
			  public function adminpost($retour) {
			    $posts = $this->post->getposts();
			    $vue = new View("AdminPost");
			    $vue->generer(array('posts' => $posts, 'message' => $retour));
			  }
			  //Modifier un post
			  public function uppost($idPost, $titre, $contenu){
			  	//sauvegarde du post
			  	$retour = $this->post->uppost($idPost, $titre, $contenu);
			    if($retour){
			    	//refresh de l'affichage du post
			  	   $this->adminpost("post modifié");
			    }else{
			    	$this->adminpost("erreur");
			    }
			  }
			  //Afficher l'éditeur
			  public function editeurpost($idPost){
			  	$post = $this->post->getpost($idPost);
			  	$vue = new View("UpPost");
				$vue->generer(array('post' => $post));
				}
	}
