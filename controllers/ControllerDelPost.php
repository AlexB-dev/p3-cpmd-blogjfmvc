<?php
	require_once 'models/Post.php';
	require_once 'views/view.php';

class ControllerDelPost {
  private $post;

  public function __construct() {
    $this->post = new Post();
  }
 // Affiche la liste de tous les Posts du blog
  public function Post() {
	  $posts = $this->post->getPosts();
	  $vue = new View("AdminPost");
	  $vue->generer(array('posts' => $posts));
	}

//Supprimer un Post

	 public function delPost($idPost){
	 	$this->post->delPost($idPost);
	 	//Actualisation de la page d'administration
	 	$this->Post();
	 }
}