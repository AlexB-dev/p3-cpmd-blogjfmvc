<?php
require_once 'views/view.php';
require_once 'controllers/ControllerPost.php';
class ControllerFront{
  private $ctrlPost;

  function __construct($data){
  $this->data = $data;
  $this->ctrlPost = new ControllerPost;
 }

  function do(){

  switch ($this->data["seg2"]){
    case "post": 
          $idPost = $this->data['id'];
          $this->ctrlPost->Post($idPost);
          break;
    case "commenter" : 
          $auteur = $this->data['auteur'];
          $contenu = $this->data['contenu'];
          $idPost = $this->data['id'];
          $this->ctrlPost->commenter($auteur, $contenu, $idPost);
           break; 
    case "signalcomm":
          $idPost = $this->data['idpost'];
          $idCommentaire = $this->getParametre($_POST, 'id');
          $this->ctrlPost->upStatCommSignal($idCommentaire, $idPost);
          break;   

  }
 }
   // Recherche un paramètre dans un tableau
  private function getParametre($tableau, $nom) {
    if (isset($tableau[$nom])) {
      return $tableau[$nom];
    }
    else
      throw new Exception("Paramètre '$nom' absent");
  }
}