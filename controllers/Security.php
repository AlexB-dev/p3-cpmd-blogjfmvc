<?php
class Security {

 public function __construct(){
  		session_start();
  		if (!isset($_SESSION['token'])){	
  			$_SESSION['token'] = base64_encode( bin2hex(random_bytes(32)));
  		}
  	}
 public function secureData(){
 			$data=[];
 		if (!empty ($_POST)){

	
  		//On vérifie que tous les jetons sont là
	  	if (!empty($_POST['token'])) {
	    // On vérifie que les deux correspondent
	    	if ($_SESSION['token'] == $_POST['token']) {
	        // Vérification terminée
	        }
		}else{
	    // Les token ne correspondent pas
	    // On ne supprime pas
	    echo $_POST['token'];
	    die ("Erreur de vérification <br/>". $_SESSION['token'] ."<br/>". $_POST['token']);
	 }
	  // if (!empty($_GET)){
   //    $tmp = explode ( "/", filter_var($_GET["urlReq"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW));
   //   }
	}
	 if(isset($_POST['login']))   $data['login']  = filter_input(INPUT_POST,'login', FILTER_SANITIZE_STRING);
	 if(isset($_POST['pass']))    $data['pass']   = filter_input(INPUT_POST,'pass', FILTER_SANITIZE_STRING);
	 if(isset($_POST['auteur']))  $data['auteur'] = filter_input(INPUT_POST,'auteur', FILTER_SANITIZE_STRING);
	 if(isset($_GET['seg3']))     $data['id'] 	  = filter_input(INPUT_GET,'seg3',FILTER_SANITIZE_NUMBER_INT);
	 if(isset($_POST['idCommentaire'])) $data['id'] = filter_input(INPUT_POST,'idCommentaire',FILTER_SANITIZE_NUMBER_INT);
	 if(isset($_POST['idpost']))  $data['idpost']  = filter_input(INPUT_POST,'idpost',FILTER_SANITIZE_NUMBER_INT);
	 if(isset($_GET['seg2']))     $data['seg2']   = filter_input(INPUT_GET,'seg2', FILTER_SANITIZE_STRING);
	 if(isset($_GET['seg1']))     $data['seg1']   = filter_input(INPUT_GET,'seg1',FILTER_SANITIZE_STRING);
	 if(isset($_POST['titre']))   $data['titre']  = filter_input(INPUT_POST,'titre',FILTER_SANITIZE_STRING);
	 if(isset($_POST['contenu'])) $data['contenu']= filter_input(INPUT_POST,'contenu',FILTER_SANITIZE_STRING);
	 if(isset($_POST['token'])) $data['token']    = filter_input(INPUT_POST,'token', FILTER_SANITIZE_STRING);
	 if(isset($_SESSION['Auth'])) $data['Auth']   = filter_var($_SESSION['Auth'], FILTER_SANITIZE_STRING);
	 return $data;
	}
}