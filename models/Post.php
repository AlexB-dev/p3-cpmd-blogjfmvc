<?php

require_once 'models/Model.php';

class Post extends Model {

  // Renvoie la liste des billets du blog
  public function getPosts() {
    $sql = 'select BIL_ID as id, BIL_DATE as date,'
      . ' BIL_TITRE as titre, BIL_CONTENU as contenu from t_billet'
      . ' order by BIL_ID desc';
    $posts = $this->executerRequete($sql);
    return $posts;
  }

  // Renvoie les informations sur un billet
  public function getPost($idPost) {
    $sql = 'select BIL_ID as id, BIL_DATE as date,'
      . ' BIL_TITRE as titre, BIL_CONTENU as contenu from t_billet'
      . ' where BIL_ID=?';
    $post = $this->executerRequete($sql, array($idPost));
    if ($post->rowCount() == 1)
      return $post->fetch();  // Accès à la première ligne de résultat
    else
      throw new Exception("Aucun billet ne correspond à l'identifiant '$idPost'");
    }

 // Ajouter un billet dans la bdd
  public function addPost($titre, $contenu){
    $sql = 'INSERT INTO t_billet (BIL_TITRE, BIL_CONTENU, BIL_DATE)'
      . ' values(?, ?, ?)';
    $date = date('Y-m-d H:i:s');  // Récupère la date courante
    try{
    $this->executerRequete($sql, array($titre, $contenu, $date));
    return true; 
   }catch(Exception $e){
   return $this->erreur($e->getMessage());
   }
  }
// Modifier un billet dans la bdd
  public function upPost($idPost, $titre, $contenu){ try{
    $sql = "UPDATE t_billet SET BIL_TITRE=:titre, BIL_CONTENU=:contenu WHERE BIL_ID=:idPost";
    $params = array('titre' => $titre, 'contenu' => $contenu, 'idPost' => $idPost);
    $post = $this->executerRequete($sql, $params);
    return true;
    }catch(Exception $e){
      return false;
    }
  }
    // Supprimer un billet de la bdd
  public function delPost($idPost){ try{
    $sql = "DELETE FROM t_billet WHERE BIL_ID=:idPost";
    $params = array('idPost' => $idPost);
    $post = $this->executerRequete($sql, $params);
    return true;
   }catch(Exception $e){
    return false;
  }
 }
}