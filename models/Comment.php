<?php

require_once 'models/Model.php';

class Commentaire extends Model {

  // Renvoie la liste des commentaires associés à un billet
  public function getCommentaires($idPost) {
    $sql = 'SELECT COM_ID as id, COM_DATE as date,'
      . ' COM_AUTEUR as auteur, COM_CONTENU as contenu from t_commentaire'
      . ' WHERE BIL_ID=? AND COM_STAT > 1';
    $commentaires = $this->executerRequete($sql, array($idPost));
    return $commentaires;

  }
  // Renvoie la liste des commentaires à modérer
  public function getModerateCommentaires() {
    $sql = 'SELECT COM_ID as id, COM_DATE as date,'
      . ' COM_AUTEUR as auteur, COM_CONTENU as contenu from t_commentaire'
      . ' WHERE COM_STAT < 2';
    $commentaires = $this->executerRequete($sql);
    return $commentaires;

  }
  //Mise à jour du statu du commentaire
  public function upStatCommentaire($idCommentaire){ try {
    $sql = "UPDATE t_commentaire SET COM_STAT ='2' WHERE COM_ID=:idCommentaire";
    $params = array('idCommentaire' => $idCommentaire);
    $commentaires = $this->executerRequete($sql, $params);
    return true;
   }catch(Exception $e){
    return false;
  }
 }
   //Mise à jour du statu du commentaire (signaler)
  public function upStatCommSignal($idCommentaire){ try {
    $sql = "UPDATE t_commentaire SET COM_STAT ='1' WHERE COM_ID=:idCommentaire";
    $params = array('idCommentaire' => $idCommentaire);
    $commentaires = $this->executerRequete($sql, $params);
    return true;
   }catch(Exception $e){
    return false;
  }
 }
  // Ajoute un commentaire dans la base
  public function ajouterCommentaire($auteur, $contenu, $idPost) {
    $sql = 'INSERT INTO t_commentaire(COM_DATE, COM_AUTEUR, COM_CONTENU, BIL_ID)'
      . ' values(?, ?, ?, ?)';
    $date = date("y-m-d H:i:s",time());  // Récupère la date courante
    $this->executerRequete($sql, array($date, $auteur, $contenu, $idPost));
  }
}

/*

0 : à valider;
1 : commentaire signalé;
2 : validé par l'admin;
3 : signalé mais validé ;

*/