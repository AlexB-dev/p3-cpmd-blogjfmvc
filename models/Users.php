<?php
require_once 'models/Model.php';

class Users extends Model {

    public function verifUser($login, $pass){
        $pass = hash('ripemd160', $pass);
        $sql = $this->executerRequete("SELECT id FROM t_users WHERE login=:login AND pass=:pass", array('login'=>$login, 'pass'=>$pass));
        $req = $sql->fetchAll(); 
        if(count($req)==0){
            return false;
        }
         return true; 
        }
    }