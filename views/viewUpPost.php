<?php ob_start(); ?>
<h4>Attention vous allez modifier votre post</h4>
<form method="post" action="?seg1=admin&seg2=uppost&seg3=<?= $post['id'] ?>">
  <input type="hidden" name="token" value= "<?=$_SESSION['token']?>">
  <article>
      <input type="hidden" name="id" value="<?=$post['id'] ?>">
      <h1>Titre du post : <input type="text" id="titre" name="titre" value="<?= $post['titre'] ?>" /></h1>
      <time><?= $post['date'] ?></time>
    <img src="./assets/img/posts/post.jpg" class="activator materialboxed" alt="<?= $post['titre'] ?>"/>
    <textarea id="edit" name="contenu"><?= $post['contenu']?></textarea>
  </article>
  <button class="btn waves-effect waves-light savebtn" type="submit">Sauvegarder
     <i class="material-icons right">create</i>
  </button>
</form>
<form method="post" action="?seg1=admin&seg2=delpost&seg3=<?= $post['id'] ?>">
  <input type="hidden" name="token" value= "<?=$_SESSION['token']?>">
  <input type="hidden" name="id" value="<?=$post['id'] ?>">
  <a class="btn waves-effect waves-light red darken-4 delbtn modal-trigger" href="#modaldel">Supprimer
     <i class="material-icons right">clear</i>
  </a>
  <!-- Modal Structure -->
  <div id="modaldel" class="modal">
    <div class="modal-content">
      <h4>Suppression de l'article</h4>
      <p>Attention ! Vous etes sur le point de supprimer votre article</p>
    </div>
    <div class="modal-footer">
      <button class="btn waves-effect waves-light red darken-4 delbtn" href="#modaldel">Supprimer
        <i class="material-icons right">clear</i>
      </button>
    </div>
  </div> 
</form>
 
  <?php $contenu = ob_get_clean(); ?>
<div>
  <?= $contenu ?>   <!-- Élément spécifique -->
</div>
