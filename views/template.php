<!doctype html>
<html lang="fr">
  <head>
      <!--Import jQuery before materialize.js-->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="./assets/js/materialize.js"></script>
    <script src="./assets/js/script.js"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'#edit'  });</script>
    
    <meta charset="UTF-8" />
    <link rel="stylesheet" href="./assets/css/style.css" />
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Alex+Brush" rel="stylesheet">
    <link rel="stylesheet" href="./assets/css/materialize.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Blog JF - L'Alaska</title>
  </head>
  <body>
      <header>
            <nav class="grey darken-3">
                <div class="nav-wrapper">
                    <a href="index.php" class="brand-logo"><i class="material-icons titlenav">photo_album</i> Jean F </a>
                    <a href="index.php" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                  <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="index.php?seg1=admin&seg2=connexion">Se connecter</a></li>
                </ul>
              </div>
            </nav>
            <ul class="sidenav" id="mobile-demo">
                <li><a href="index.php">Accueil</a></li>
                <li><a href="index.php?seg1=admin&seg2=connexion">Se connecter</a></li>
            </ul>  
        <h2 class="titre2">Mon blog, partager avec vous mon écriture</h2><hr/>
      </header>
      <div id="contenu">
        <?= $contenu ?>   <!-- Élément spécifique -->
      </div>
      <footer class="light-blue accent-2">
        <div class="container ">
          <div class="row">
            <div class="col l6 s12">
              <h5 class="white-text">Un petit mot pour mes lecteurs...</h5>
              <p class="grey-text text-lighten-4">Je suis écrivain depuis mon plus jeune age, je suis heureux de tout partager avec vous !</p>
              <p class="signature">JF</p>
            </div>
            <div class="col l4 offset-l2 s12">
              <h5 class="white-text">Me suivre ! via :</h5>
              <ul>
                <li><a class="grey-text text-lighten-3" href="#!">Facebook</a></li> 
                <li><a class="grey-text text-lighten-3" href="#!">Twitter</a></li>    
              </ul>
            </div>
          </div>
        </div>
        <div class="footer-copyright">
          <div class="row footer">
             <p class="white-text">© 2018 Copyright Text | Développer par Alexandre BLONDEL </p>
          </div>     
        </div>
    </footer>
  </body>
</html>