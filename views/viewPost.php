<?php ob_start(); ?>
<article>
    <h1 class="titrepost"><?= $post['titre'] ?></h1>
    <time><?= $post['date'] ?></time>
  <img src="./assets/img/posts/post.jpg" class="activator materialboxed" alt="<?= $post['titre'] ?>"/>
  <p><?= $post['contenu'] ?></p>
</article>
<hr />
<div>
  <h1 id="titreReponses">Réponses à <?= $post['titre'] ?></h1>
</div>
<?php foreach ($commentaires as $commentaire): ?>
  <form method="post" action="?seg1=article&seg2=signalcomm">
    <input type="hidden" name="token" value= "<?=$_SESSION['token']?>">
    <input type="hidden" name="id" value="<?=$commentaire['id'] ?>">
    <input type="hidden" name="idpost" value="<?=$post['id'] ?>">
    <p><?= $commentaire['auteur'] ?> dit :</p>
    <p><?= $commentaire['contenu'] ?> </p>
    <input class="icon_prefix btn waves-effect waves-light" type="submit" value="Signaler">
</form>
<?php endforeach; ?>
<?php $contenu = ob_get_clean(); ?>
<div>
     <?= $contenu ?>   <!-- Élément spécifique -->
</div>

<!-- Formulaire -->

<div class="row">
  <form method="post" action="?seg1=article&seg2=commenter&seg3=<?= $post['id'] ?>" class="col s12">
    <input type="hidden" name="token" value= "<?=$_SESSION['token']?>">
     <div class="row">
        <div class="input-field col s6">
          <i class="material-icons prefix">account_circle</i>
          <input id="icon_prefix" name="auteur" type="text" class="validate" required>
          <label for="icon_prefix">Votre nom</label>
        </div>
        <div class="input-field col s6">
          <i class="material-icons prefix">chat_bubble</i>
          <textarea id="textarea1" name="contenu" class="materialize-textarea" required></textarea>
          <label for="textarea1">Votre commentaire</label>
          <input type="hidden" name="id" value="<?= $post['id'] ?>" />
        </div>
        <button class="btn waves-effect waves-light btncomm" type="submit" name="action" value="Commenter">Commenter
          <i class="material-icons right">create</i>
        </button>
    </div>
  </form>
</div>

