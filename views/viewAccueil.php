<?php foreach ($posts as $post):?>
    <div class="col l12 m12 s12">
      <div class="card">
        <div class="card-content">
          <a href="<?= "?seg1=article&seg2=post&seg3=" . $post['id'] ?>">
            <h2 class="grey-text text-darken-2 titrepost"><?=$post['titre'] ?></h2>
         </a>

         <h6 class="grey-text">Le <time><?= $post['date'] ?></time></h6>
          </div>
            <div class="card-image waves-effect waves-block waves-light">
              <img src="./assets/img/posts/post.jpg" class="activator" alt="<?= $post['titre'] ?>"/>
            </div>
          <div class="card-content">
            <span class="card-title activator grey-text text-darken-4"><i class="material-icons right">more_vert</i></span>
            <p><a href="<?="?seg1=article&seg2=post&seg3=" . $post['id']?>">Voir l'article complet</a></p>
          </div>
          <div class="card-reveal">
           <span class="card-title grey-text text-darken-4"><?= $post['titre'] ?><i class="material-icons right">close</i></span>
            <p><?= $post['contenu'] ?>...</p>
            </div>
          </div>
    </div>         
<?php endforeach; ?>
