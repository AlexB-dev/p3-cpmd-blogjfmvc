<?php $this->titre = "Blog - JF"; ?>

<h3 class="textadmin">Bienvenu dans votre interface d'administration voici vos posts : </h3>
<?php if(isset($message)){ ?>
    <div class="alert">
      <span class="closebtn" onclick="this.parentElement.style.display=' none';">&times;</span> 
      <strong>Succès</strong> Votre post à été modifié.
    </div> 
<?php } ?>
<div class="row">
  <div class ="col s12 m4 l4">
    <form method="post" action="?seg1=admin&seg2=editionaddpost">
      <input type="hidden" name="token" value= "<?=$_SESSION['token']?>">
      <button class="btn-floating btn-large waves-effect waves-light red addbtn"><i class="material-icons">add</i></button>
    </form>
  </div>
  <div class="col s12 m4 l4">
    <form method="post" action="?seg1=admin&seg2=moderationpost">
      <input type="hidden" name="token" value= "<?=$_SESSION['token']?>">
    <button class="btn waves-effect waves-light" type="submit">Modération Commentaires
      <i class="material-icons right">chat_bubble_outline</i>
    </button>
    </form>
  </div>
  <div class="col s12 m4 l4">
    <form method="post" action="?seg1=admin&seg2=deconnexion">
      <input type="hidden" name="token" value= "<?=$_SESSION['token']?>">
      <button class="btn waves-effect waves-light" type="submit">Se déconnecté
        <i class="material-icons right">person_outline</i>
     </button>
    </form>
   </div> 
</div>
<?php foreach ($posts as $post): ?>
    <div class="col l12 m12 s12">
      <form class="formadminpost" method="post" action="?seg1=admin&seg2=editionuppost&seg3=<?= $post['id'] ?>">
        <input type="hidden" name="token" value= "<?=$_SESSION['token']?>">
        <div class="card-content">
          <input type="hidden" name="id" value="<?=$post['id'] ?>">
           <h2 class="titrepostadmin"><?= $post['titre'] ?></h2>
           <button class="btn waves-effect waves-light" type="submit">Modifier
           <i class="material-icons right">create</i>
           </button>
         </div>
      </form>
    </div>         
<?php endforeach; ?>
